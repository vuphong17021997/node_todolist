var mongoose = require('mongoose')
var Schema = mongoose.Schema;

 var TaskSchema = new Schema({
     task:{
         type:String,
            default: ''
     },
     created_data:{
         type:Date,
         default:Date.now
     },
     status:{
         type:[{
             type:Boolean,
             enum:[false,true]
         }],
         default:[false]
     },
     order:{
         type:Number,
     }
 });
 module.exports = mongoose.model('task',TaskSchema)